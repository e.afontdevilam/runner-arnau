using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] gameObjects;
    public Transform[] spawnPoints; 

    void Start()
    {
        StartCoroutine(Spawnejar());
    }

    IEnumerator Spawnejar()
    {
        while (true)
        {
            int randObjectIndex = Random.Range(0, gameObjects.Length);
            int randSpawnIndex = Random.Range(0, spawnPoints.Length);

            GameObject newGameObject = Instantiate(gameObjects[randObjectIndex]);
            newGameObject.transform.position = spawnPoints[randSpawnIndex].position;

            yield return new WaitForSeconds(1f);
        }
    }
}