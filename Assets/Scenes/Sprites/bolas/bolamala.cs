using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bolamala : MonoBehaviour
{
    private Rigidbody2D rb; 

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        int randomValue = Random.Range(1, 4);

        if (randomValue == 2 || randomValue == 4)
        {
            rb.gravityScale = 1;
        }
        else if (randomValue == 3 || randomValue == 1)
        {
            rb.gravityScale = -1;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("pivote"))
        {
            rb.gravityScale = -1;
        }
        else if (collision.gameObject.CompareTag("pivoteazul"))
        {
            rb.gravityScale = 1;
        }
    }


private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("blueline"))
        {
            rb.gravityScale = -1;
            
        }
        else if (other.gameObject.CompareTag("redline"))
        {
            rb.gravityScale = 1;
            
        }
    }
}