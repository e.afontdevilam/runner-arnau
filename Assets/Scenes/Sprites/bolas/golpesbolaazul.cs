using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class golpesbolaazul : MonoBehaviour
{
    public float fuerzaEmpuje = -10f; 

    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (collision.gameObject.CompareTag("pivoteazul"))
        {
            
            Rigidbody2D rb = GetComponent<Rigidbody2D>();

           
            rb.AddForce(Vector2.up * fuerzaEmpuje);
        }
    }
}
