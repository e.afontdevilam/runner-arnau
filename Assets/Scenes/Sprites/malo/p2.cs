using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class p2 : MonoBehaviour
{
    public static int contadorColisiones = 10;
    public int spd;
    public TMP_Text textoContador2;
    private Rigidbody2D rb;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        textoContador2 = GameObject.Find("TextoContador2").GetComponent<TMP_Text>();

      
        textoContador2.text = contadorColisiones.ToString();
    }

    
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow)){
            rb.velocity = new Vector2(-spd, rb.velocity.y);
        }else if (Input.GetKey(KeyCode.RightArrow)) {
            rb.velocity = new Vector2(spd, rb.velocity.y);
        }
        else
        {
            rb.velocity = new Vector2(0, rb.velocity.y);
        }
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            rb.AddForce(new Vector2(0, 333));
        }
    }


private void OnCollisionEnter2D(Collision2D collision)
    {
       
        if (collision.gameObject.CompareTag("bolaroja"))
        {
           
            contadorColisiones--;
            textoContador2.text = contadorColisiones.ToString();

            if (contadorColisiones <= 0)
            {
               
                
               SceneManager.LoadScene("Redwin");
            }
        } else if (collision.gameObject.CompareTag("bolamala")){
            contadorColisiones--;
            textoContador2.text = contadorColisiones.ToString();

            if (contadorColisiones <= 0)
            {
                contadorColisiones = 10;
                SceneManager.LoadScene("Redwin");
            }

        }
    }


}



/* if (contadorColisiones >= 10)
            {
                // Congelar la pantalla durante 5 segundos
                Time.timeScale = 0f;
                StartCoroutine(DestruirObjetoYCambiarEscena());
            }
        }
    }

    IEnumerator DestruirObjetoYCambiarEscena()
    {
        // Esperar 5 segundos en tiempo real
        yield return new WaitForSecondsRealtime(5f);

        // Descongelar la pantalla
        Time.timeScale = 1f;

        // Destruir el objeto
        Destroy(objetoADestruir);

        // Cambiar de escena
        SceneManager.LoadScene(nombreDeEscena);
    }
} */
