using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlizquierdabuena : MonoBehaviour
{
   public float rotationAmount = -70f; 
    public float returnAmount = 70f; 
    public float rotationSpeed = 100f; 

    private bool isRotating = false; 

    void Update()
    {
       
        if (Input.GetKeyDown(KeyCode.W) && !isRotating)
        {
            isRotating = true;
            StartCoroutine(RotateLever(rotationAmount));
        }

        
        if (Input.GetKeyUp(KeyCode.W) && isRotating)
        {
            isRotating = false;
            StartCoroutine(RotateLever(returnAmount));
        }
    }

    private System.Collections.IEnumerator RotateLever(float targetRotation)
    {
        float currentRotation = 0f;
        while (Mathf.Abs(currentRotation) < Mathf.Abs(targetRotation))
        {
            float rotationStep = rotationSpeed;
            if (targetRotation > 0)
            {
                transform.Rotate(Vector3.forward * rotationStep);
                currentRotation += rotationStep;
            }
            else
            {
                transform.Rotate(Vector3.back * rotationStep);
                currentRotation -= rotationStep;
            }
            yield return null;
        }
    }
}