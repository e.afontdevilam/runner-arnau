using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class p1 : MonoBehaviour
{
    
    public static int contadorColisiones = 10;
    
    public int spd;
    
    private Rigidbody2D rb;

    
    public TMP_Text textoContador;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

      
        textoContador = GameObject.Find("TextoContador").GetComponent<TMP_Text>();

        
        textoContador.text = contadorColisiones.ToString();
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            rb.velocity = new Vector2(-spd, rb.velocity.y);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            rb.velocity = new Vector2(spd, rb.velocity.y);
        }
        else
        {
            rb.velocity = new Vector2(0, rb.velocity.y);
        }
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            rb.AddForce(new Vector2(0, 333));
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("bolaazul"))
        {
            contadorColisiones--;

            
            textoContador.text =contadorColisiones.ToString();

            if (contadorColisiones <= 0)
            {
                SceneManager.LoadScene("Bluewin");
            }
        }
        else if (collision.gameObject.CompareTag("bolamala"))
        {
            contadorColisiones --;

           
            textoContador.text = contadorColisiones.ToString();

            if (contadorColisiones <= 0)
            {
                contadorColisiones = 10;
                SceneManager.LoadScene("Bluewin");

            }
        }
    }
}



//transform.Rotate(0,0,90)